// Upgrade NOTE: upgraded instancing buffer 'Props' to new syntax.

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/AlwaysOnTopText"{
    Properties{
        _MainTex("Font Texture", 2D) = "white" {}
        _Color("Text Color", Color) = (1,0,0,1)

    }

    SubShader{

        Tags { 
            "Queue" = "Transparent" 
            "IgnoreProjector" = "True" 
            "RenderType" = "Transparent" 
        }

        Lighting Off
        ZWrite off
        ZTest Off
        Cull off
        Fog { Mode Off }

        Blend SrcAlpha OneMinusSrcAlpha

        Pass {
            Color[_Color] // not used
            ColorMaterial AmbientAndDiffuse // use material colour which includes vertex colour

            SetTexture[_MainTex] {
                combine primary, texture * primary

            }
        }
    }
}