using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenCloseDoor : MonoBehaviour
{
    public Animator animator;
    public AudioSource audio;

    public void openDoor()
    {
        GameObject.Find("Tutorial").GetComponent<Tutorial>().startEndState();

        animator.SetTrigger("open");
        audio.Play();
    }

    public void closeDoor()
    {
        animator.SetTrigger("close");
        audio.Play();
    }
}
