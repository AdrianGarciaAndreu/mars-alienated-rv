using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public GameObject cameraPointer;

    private Vector3 jump;
    private float distance = 0.6f;

    public void launchJump(string name)
    {
        switch (name)
        {
            case "front":
                jump = new Vector3(distance, 0, 0);
                break;
            case "back":
                jump = new Vector3(-distance, 0, 0);
                break;
            case "left":
                jump = new Vector3(0, 0, distance);
                break;
            case "right":
                jump = new Vector3(0, 0, -distance);
                break;
        }

        cameraPointer.GetComponent<CameraPointer>().isMoving = true;
        transform.position = new Vector3(jump.x + transform.position.x, transform.position.y, jump.z + transform.position.z);
        cameraPointer.GetComponent<CameraPointer>().isMoving = false;

        GameObject.Find("Tutorial").GetComponent<Tutorial>().startUseZoomState();
    }
}
