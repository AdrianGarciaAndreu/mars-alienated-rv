using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ReceiveHit : MonoBehaviour
{
    public UnityEvent myEvent;

    public void OnPointerEnter()
    {
        if (Input.GetButtonDown("A"))
        {
            myEvent.Invoke();
        }

    }
    public void OnPointerExit()
    {
    }
    public void OnPointerClick()
    {
    }
}
