using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CodePanelButtonInteraction : MonoBehaviour
{
    private string completeText = "";
    private GameObject tempButton;

    public string correctPin;
    public GameObject door;
    public CameraPointer cp;
    public Text codeText;

    public ZoomToObject zoomSection;

    public AudioSource audio;
    public AudioClip codePanelClick;
    public AudioClip codePanelCorrect;
    public AudioClip codePanelError;

    public void setNumberIntoPanel(GameObject obj)
    {

        tempButton = obj;

        StartCoroutine(clickButton());

        string number = obj.name.Substring(1);
        completeText += number;

        codeText.text = completeText;

        if (this.completeText.Length == 4)
        {
            checkIfPinIsCorrect();
        }
    }

    void checkIfPinIsCorrect()
    {
        if (correctPin == completeText)
        {
            Debug.Log("EL PIN ES CORRECTO");

            audio.clip = codePanelCorrect;
            audio.Play();

            completeText = "";
            door.GetComponent<OpenCloseDoor>().openDoor();
            StartCoroutine(quitZoom());
        }
        else
        {
            audio.clip = codePanelError;
            audio.Play();

            Debug.Log("EL PIN ES ERRONEO");
            completeText = "";
        }

        codeText.text = "****";
    }

    IEnumerator quitZoom()
    {
        yield return new WaitForSeconds(1);

        GameObject.Find("Tutorial").GetComponent<Tutorial>().startExitZoomState();

        zoomSection.setOriginalCameraPosition();
    }

    IEnumerator clickButton()
    {
        GameObject btn = tempButton.transform.GetChild(0).gameObject;

        btn.SetActive(true);

        audio.clip = codePanelClick;
        audio.Play();

        yield return new WaitForSeconds(1);

        btn.SetActive(false);
    }
}
