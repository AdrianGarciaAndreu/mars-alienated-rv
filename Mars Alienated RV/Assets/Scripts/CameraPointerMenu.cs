using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraPointerMenu : MonoBehaviour
{
    private const float _maxDistance = 10;
    private GameObject _gazedAtObject = null;
    public GameObject canvas;
    public GameObject eyehole;

    public GameObject transitionObject;
    private bool objectDetected = false;
   
    private int counter = 3; // counter in secs to confirm action
    private bool actionConfirmed = false;

    
    public bool isMoving = false;
    // Update is called once per frame
    void Update()
    {

        int layerMask;
        layerMask = 1 << LayerMask.NameToLayer("receiver");

        // Casts ray towards camera's forward direction, to detect if a GameObject is being gazed
        // at.
        RaycastHit hit;
        Debug.DrawLine(transform.position, transform.position + transform.forward * _maxDistance, Color.red);
        if (Physics.Raycast(transform.position, transform.forward, out hit, _maxDistance, layerMask))
        {
            // GameObject detected in front of the camera.
            if (((_gazedAtObject == hit.transform.gameObject) && !isMoving) || (_gazedAtObject != hit.transform.gameObject))
            {

                // New GameObject.
                _gazedAtObject = hit.transform.gameObject;
                if (_gazedAtObject.name == "collider_play" && objectDetected == false)
                {

                    objectDetected = true;
                    Debug.Log("Play Game");
                    Animation fadeOut = transitionObject.GetComponent<Animation>();
                    fadeOut.Play("FadeOut");
                    StartCoroutine(MakeTheLoad("IntroScene"));
                }

                if (_gazedAtObject.name == "collider_exit" && objectDetected == false) {
                    objectDetected = true;

                    Debug.Log("Quit Game");
                    Application.Quit();
                }

            }
        }
        else
        {
            Debug.Log("Stoped");
            objectDetected = false;
            _gazedAtObject = null;
        }

        // Checks for screen touches.
        if (Input.GetButtonDown("A"))
        {
            _gazedAtObject?.SendMessage("OnPointerClick");
        }

        canvas.GetComponent<RectTransform>().position = transform.position + transform.forward * _maxDistance;
    }

    void resetAnimationToSmallEyehole()
    {
        eyehole.GetComponent<Animator>().SetTrigger("normal");
        eyehole.GetComponent<Animator>().SetBool("focus", false); 
        eyehole.GetComponent<Animator>().SetBool("focusButtonA", false);
        eyehole.GetComponent<Animator>().SetBool("focusButtonZoom", false);
    }

    void checkObjectToSetAnimation(string tag)
    {
        if(tag == "Zoom")
        {
            if (!eyehole.GetComponent<Animator>().GetBool("focusButtonZoom"))
            {
                eyehole.GetComponent<Animator>().ResetTrigger("normal");
                eyehole.GetComponent<Animator>().SetBool("focusButtonZoom", true);
            }
        }
        else
        {
            if (!eyehole.GetComponent<Animator>().GetBool("focusButtonA"))
            {
                eyehole.GetComponent<Animator>().ResetTrigger("normal");
                eyehole.GetComponent<Animator>().SetBool("focusButtonA", true);
            }
        }
    }




    // Tieme to load
    IEnumerator MakeTheLoad(string scene)
    {
        yield return new WaitForSeconds(1f);
        AsyncOperation operation = SceneManager.LoadSceneAsync(scene);

        while (operation.isDone == false)
        {
            yield return null;
        }
    }







}

