using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPointer : MonoBehaviour
{
    private const float _maxDistance = 3;
    private GameObject _gazedAtObject = null;
    public GameObject canvas;
    public GameObject eyehole;

    public bool isMoving = false;

    // Update is called once per frame
    void Update()
    {
        int layerMask;
        layerMask = 1 << LayerMask.NameToLayer("receiver");

        // Casts ray towards camera's forward direction, to detect if a GameObject is being gazed
        // at.
        RaycastHit hit;
        Debug.DrawLine(transform.position, transform.position + transform.forward * _maxDistance, Color.red);
        if (Physics.Raycast(transform.position, transform.forward, out hit, _maxDistance, layerMask))
        {
            // GameObject detected in front of the camera.
            if (((_gazedAtObject == hit.transform.gameObject) && !isMoving) || (_gazedAtObject != hit.transform.gameObject))
            {
                // New GameObject.
                _gazedAtObject?.SendMessage("OnPointerExit");
                _gazedAtObject = hit.transform.gameObject;
                _gazedAtObject.SendMessage("OnPointerEnter");
                checkObjectToSetAnimation(hit.transform.gameObject.tag);
            }
        }
        else
        {
            // No GameObject detected in front of the camera.
            _gazedAtObject?.SendMessage("OnPointerExit");
            _gazedAtObject = null;
            resetAnimationToSmallEyehole();
        }

        // Checks for screen touches.
        if (Input.GetButtonDown("A"))
        {
            _gazedAtObject?.SendMessage("OnPointerClick");
        }

        canvas.GetComponent<RectTransform>().position = transform.position + transform.forward * _maxDistance;
    }

    void resetAnimationToSmallEyehole()
    {
        eyehole.GetComponent<Animator>().SetTrigger("normal");
        eyehole.GetComponent<Animator>().SetBool("focus", false); 
        eyehole.GetComponent<Animator>().SetBool("focusButtonA", false);
        eyehole.GetComponent<Animator>().SetBool("focusButtonZoom", false);
    }

    void checkObjectToSetAnimation(string tag)
    {
        if(tag == "Zoom")
        {
            if (!eyehole.GetComponent<Animator>().GetBool("focusButtonZoom"))
            {
                eyehole.GetComponent<Animator>().ResetTrigger("normal");
                eyehole.GetComponent<Animator>().SetBool("focusButtonZoom", true);
            }
        }
        else
        {
            if (!eyehole.GetComponent<Animator>().GetBool("focusButtonA"))
            {
                eyehole.GetComponent<Animator>().ResetTrigger("normal");
                eyehole.GetComponent<Animator>().SetBool("focusButtonA", true);
            }
        }
    }
}
