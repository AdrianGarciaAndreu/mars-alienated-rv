using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class changeScene : MonoBehaviour
{
    // name of the next scene
    public static string nextScene;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // function to load the loading and past the name of the scene
    public void change( string name)
    {
        nextScene = name;
        SceneManager.LoadScene("loading");
    }
}
