using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class loading : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        // catch tha name of the load scena
        string ScenetoLoad = changeScene.nextScene;
        StartCoroutine(this.MakeTheLoad(ScenetoLoad));
    }

    // Update is called once per frame

    void Update()
    {
        
    }

    // Tieme to load
    IEnumerator MakeTheLoad(string scene)
    {
        yield return new WaitForSeconds(1f);
        AsyncOperation operation = SceneManager.LoadSceneAsync(scene);

        while (operation.isDone == false)
        {
            yield return null;
        }
    }
}
